package rs.itcentar.dnevnik.db.data;

public class DiaryEntry {
    private final int id;
    private long created;
    private String text;
    private boolean locked;
    private final int userId;

    public DiaryEntry(int id, long created, String text, boolean locked, int userId) {
        this.id = id;
        this.created = created;
        this.text = text;
        this.locked = locked;
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public long getCreated() {
        return created;
    }

    public String getText() {
        return text;
    }

    public boolean isLocked() {
        return locked;
    }

    public int getUserId() {
        return userId;
    }

    @Override
    public String toString() {
        return "Entry{" + "id=" + id + ", created=" + created + ", text=" + text + ", locked=" + locked + ", userId=" + userId + '}';
    }
}
