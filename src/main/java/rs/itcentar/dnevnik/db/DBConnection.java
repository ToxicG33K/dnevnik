package rs.itcentar.dnevnik.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Java
 */
public class DBConnection {
    private static final DBConfig config = new DBConfig();
    
    public static final Connection getConnection() {
        try {
//            Class.forName(config.getJDBCDriver()).newInstance();
            return DriverManager.getConnection(config.getJDBCUrl(),
                    config.getJDBCUser(),
                    config.getJDBCPassword());
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
