package rs.itcentar.dnevnik;

public class WindowManager {
    private static LoginFrame loginFrame;

    public static LoginFrame getLoginFrame() {
        return loginFrame;
    }

    public static void setLoginFrame(LoginFrame loginFrame) {
        WindowManager.loginFrame = loginFrame;
    }
}
