package rs.itcentar.dnevnik.db.repositories;

import com.google.common.hash.Hashing;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import rs.itcentar.dnevnik.db.DBConnection;
import rs.itcentar.dnevnik.db.data.User;

/*
- nadji korisnika po username - getUserByUsername()
- nadji korisnika po id - getUserById()
- proveri da li korisnik sa username postoji - getUserByUsername() i/ili getUserById()
- proveri da li je password dobar - checkPasswordForUsername()
- napravi korisnika - createUser()
- brisanje korisnika - deleteUser() i/ili deleteUserById()
 */
public class UsersRepository {

    public static User getUserByUsername(String username) {
        try (Connection connection = DBConnection.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM users WHERE username = ? LIMIT 1");
            statement.setString(1, username);
            try (ResultSet results = statement.executeQuery()) {
                if(results.next()) {
                    int id = results.getInt("id");
                    String name = results.getString("username");
                    String password = results.getString("password");
                    User user = new User(id, name, password);
                    return user;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsersRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static User getUserById(int id) {
        try (Connection connection = DBConnection.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM users WHERE id = ? LIMIT 1");
            statement.setInt(1, id);
            try (ResultSet results = statement.executeQuery()) {
                if(results.next()) {
                    int uid = results.getInt("id");
                    String name = results.getString("username");
                    String password = results.getString("password");
                    User user = new User(uid, name, password);
                    return user;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsersRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean checkPasswordForUsername(String username, String password) {
        try (Connection connection = DBConnection.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT password FROM users WHERE username = ? LIMIT 1");
            statement.setString(1, username);
            try (ResultSet results = statement.executeQuery()) {
                if(results.next()) {
                    String pass = results.getString("password");
                    if(pass.equalsIgnoreCase(getMD5FromPassword(password))) {
                        return true;
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsersRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    public static boolean createUser(String username, String password) throws IllegalArgumentException {
        User user = getUserByUsername(username);
        if(user == null) {
            try (Connection connection = DBConnection.getConnection()) {
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO users(username, password) VALUES (?, ?)");
                statement.setString(1, username);
                statement.setString(2, getMD5FromPassword(password));
                int ret = statement.executeUpdate();
                return ret == 1;
            } catch (SQLException ex) {
                Logger.getLogger(UsersRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // baci gresku da korisnik vec postoji
            throw new IllegalArgumentException(String.format("User '%s' already exists!", username));
        }
        return false;
    }
    
    public static boolean deleteUser(String username) throws IllegalArgumentException {
        User user = getUserByUsername(username);
        if(user != null) {
            try (Connection connection = DBConnection.getConnection()) {
                PreparedStatement statement = connection.prepareStatement(
                        "DELETE FROM users WHERE username = ?");
                statement.setString(1, username);
                int ret = statement.executeUpdate();
                return ret == 1;
            } catch (SQLException ex) {
                Logger.getLogger(UsersRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // baci gresku da korisnik vec postoji
            throw new IllegalArgumentException(String.format("User '%s' doesn't exist!", username));
        }
        return false;
    }
    
    public static boolean deleteUserById(int id) throws IllegalArgumentException {
        User user = getUserById(id);
        if(user != null) {
            try (Connection connection = DBConnection.getConnection()) {
                PreparedStatement statement = connection.prepareStatement(
                        "DELETE FROM users WHERE id = ?");
                statement.setInt(1, id);
                int ret = statement.executeUpdate();
                return ret == 1;
            } catch (SQLException ex) {
                Logger.getLogger(UsersRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // baci gresku da korisnik vec postoji
            throw new IllegalArgumentException(String.format("User with id of '%d' doesn't exist!", id));
        }
        return false;
    }
    
    private static String getMD5FromPassword(String password) {
        return Hashing.md5().hashString(password, StandardCharsets.UTF_8).toString();
    }
}
