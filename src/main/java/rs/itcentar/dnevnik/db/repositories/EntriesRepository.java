package rs.itcentar.dnevnik.db.repositories;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import rs.itcentar.dnevnik.db.DBConnection;
import rs.itcentar.dnevnik.db.data.DiaryEntry;
import rs.itcentar.dnevnik.db.data.User;

/*
- vrati listu entrija za korisnika po id - getAllEntriesForUserId()
- nadji entry po id - getEntryById()
- ubaci novi entry - createEntryForUserId()
- brisi entry - deleteEntryById()
- izmeni entry - updateEntry()
- lock/unlock entry - toggleLockEntry()
*/
public class EntriesRepository {
    
    public static List<DiaryEntry> getAllEntriesForUserId(int userId) {
        try (Connection connection = DBConnection.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM entries WHERE id_user = ?");
            statement.setInt(1, userId);
            try (ResultSet results = statement.executeQuery()) {
                List<DiaryEntry> entriesForUser = new ArrayList<>();
                while(results.next()) {
                    int eid = results.getInt("id");
                    long created = results.getLong("created");
                    String text = results.getString("text");
                    boolean locked = results.getBoolean("locked");
                    int eUserId = results.getInt("id_user");
                    DiaryEntry entry = new DiaryEntry(eid, created, text, locked, eUserId);
                    entriesForUser.add(entry);
                }
                return entriesForUser;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EntriesRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Collections.EMPTY_LIST;
    }
    
    public static DiaryEntry getEntryById(int id) {
        try (Connection connection = DBConnection.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(
                    "SELECT * FROM entries WHERE id = ? LIMIT 1");
            statement.setInt(1, id);
            try (ResultSet results = statement.executeQuery()) {
                if(results.next()) {
                    int eid = results.getInt("id");
                    long created = results.getLong("created");
                    String text = results.getString("text");
                    boolean locked = results.getBoolean("locked");
                    int eUserId = results.getInt("id_user");
                    DiaryEntry entry = new DiaryEntry(eid, created, text, locked, eUserId);
                    return entry;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(UsersRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public static boolean createEntryForUserId(int userId, String text) throws IllegalArgumentException {
        User user = UsersRepository.getUserById(userId);
        if(user != null) {
            try (Connection connection = DBConnection.getConnection()) {
                PreparedStatement statement = connection.prepareStatement(
                        "INSERT INTO entries(created, text, locked, id_user) VALUES (?, ?, ?, ?)");
                statement.setLong(1, System.currentTimeMillis());
                statement.setString(2, text);
                statement.setBoolean(3, false);
                statement.setInt(4, userId);
                int ret = statement.executeUpdate();
                return ret == 1;
            } catch (SQLException ex) {
                Logger.getLogger(EntriesRepository.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            // baci gresku da korisnik vec postoji
            throw new IllegalArgumentException(String.format("User with id of '%d' doesn't exist!", userId));
        }
        return false;
    }

    public static boolean deleteEntryById(int userId, int entryId) throws IllegalArgumentException {
        DiaryEntry entry = getEntryById(entryId);
        if(entry != null) {
            if(userId == entry.getUserId()) {
                try (Connection connection = DBConnection.getConnection()) {
                    PreparedStatement statement = connection.prepareStatement(
                            "DELETE FROM entries WHERE id = ?");
                    statement.setInt(1, entryId);
                    int ret = statement.executeUpdate();
                    return ret == 1;
                } catch (SQLException ex) {
                    Logger.getLogger(EntriesRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                throw new IllegalArgumentException(
                        String.format("Access Denied for user with id '%d' on entry with id '%d'!",
                                userId, entry.getId()));
            }
        }
        return false;
    }
    
    public static boolean updateEntry(int userId, DiaryEntry entry) {
        DiaryEntry dbEntry = getEntryById(entry.getId());
        if(dbEntry != null) {
            if(userId == dbEntry.getUserId()) {
                try (Connection conn = DBConnection.getConnection()) {
                    try (PreparedStatement ps = conn.prepareStatement(
                            "UPDATE entries SET text = ?, locked = ? WHERE id = ?")) {
                        ps.setString(1, entry.getText());
                        ps.setBoolean(2, entry.isLocked());
                        ps.setInt(3, entry.getId());
                        int ret = ps.executeUpdate();
                        return ret == 1;
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(EntriesRepository.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                throw new IllegalArgumentException(
                        String.format("Access Denied for user with id '%d' on entry with id '%d'!",
                                userId, entry.getId()));
            }
        }
        
        return false;
    }
    
    public static boolean toggleLockEntry(int id, boolean lock) {
        try (Connection conn = DBConnection.getConnection()) {
            try (PreparedStatement ps = conn.prepareStatement(
                    "UPDATE entries SET locked = ? WHERE id = ?")) {
                ps.setBoolean(1, lock);
                ps.setInt(2, id);
                int ret = ps.executeUpdate();
                return ret == 1;
            }
        } catch (SQLException ex) {
            Logger.getLogger(EntriesRepository.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
