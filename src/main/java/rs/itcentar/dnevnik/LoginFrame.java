package rs.itcentar.dnevnik;

import javax.swing.JOptionPane;
import rs.itcentar.dnevnik.db.repositories.UsersRepository;

public class LoginFrame extends javax.swing.JFrame {
    
    public LoginFrame() {
        WindowManager.setLoginFrame(LoginFrame.this);
        
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        tfUsername = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        bLogin = new javax.swing.JButton();
        bClose = new javax.swing.JButton();
        pfPassword = new javax.swing.JPasswordField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Username:");

        jLabel2.setText("Password:");

        bLogin.setText("Login");
        bLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bLoginActionPerformed(evt);
            }
        });

        bClose.setText("Close");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(tfUsername)
                            .addComponent(pfPassword, javax.swing.GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(bLogin)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(bClose)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(tfUsername, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(pfPassword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bClose)
                    .addComponent(bLogin))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bLoginActionPerformed
//        try {
//            boolean isUserCreated = UsersRepository.createUser(tfUsername.getText(),
//                    String.valueOf(pfPassword.getPassword()));
//
//            if(isUserCreated) {
//                JOptionPane.showMessageDialog(
//                    LoginFrame.this,
//                    String.format("User '%s' created!", tfUsername.getText()),
//                    "Creating User",
//                    JOptionPane.INFORMATION_MESSAGE);
//            }
//        } catch(IllegalArgumentException ex) {
//            JOptionPane.showMessageDialog(
//                    LoginFrame.this,
//                    ex.getMessage(),
//                    "Error Creating User",
//                    JOptionPane.ERROR_MESSAGE);
//        }
//        try {
//            boolean isUserDeleted = UsersRepository.deleteUser(tfUsername.getText());
//            if(isUserDeleted) {
//                System.out.println("DELETED");
//            } else {
//
//            }
//        } catch(IllegalArgumentException ex) {
//            System.err.println(ex.getMessage());
//        }
//        try {
//            boolean isUserDeleted = UsersRepository.deleteUserByID(7);
//            if(isUserDeleted) {
//                System.out.println("DELETED");
//            }
//        } catch(IllegalArgumentException ex) {
//            System.err.println(ex.getMessage());
//        }
//        List<DiaryEntry> entries = EntriesRepository.getAllEntriesForUserId(2);
//        System.out.println(entries);
//        boolean isEntryCreated = EntriesRepository.createEntryForUserId(2, "Bla bla bla");
//        if(isEntryCreated) {
//            System.out.println("CREATED");
//        }
//        boolean isEntryDeleted = EntriesRepository.deleteEntryById(2, 4);
//        if(isEntryDeleted) {
//            System.out.println("DELETED");
//        }
//        boolean isEntryUpdated = EntriesRepository.updateEntry(2, new DiaryEntry(5, System.currentTimeMillis(), "tra la la", true, 2));
//        if(isEntryUpdated) {
//            System.out.println("UPDATED");
//        }
        
        boolean isPassOK = UsersRepository.checkPasswordForUsername(tfUsername.getText(),
                String.valueOf(pfPassword.getPassword()));
        if(isPassOK) {
            // otvori DnevnikFrame
            DnevnikFrame df = new DnevnikFrame();
            df.setVisible(true);
            setVisible(false);
        } else {
            // poruka o gresci
            JOptionPane.showMessageDialog(
                    LoginFrame.this,
                    "Wrong username and/or password!",
                    "Error Loging In",
                    JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_bLoginActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bClose;
    private javax.swing.JButton bLogin;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPasswordField pfPassword;
    private javax.swing.JTextField tfUsername;
    // End of variables declaration//GEN-END:variables
}
